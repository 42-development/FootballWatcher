package models

class User(val login: String, val password: String) {
  override def toString = {
    "login: " + login + "; password: " + password
  }
}