package controllers

import accountService.AccountService
import controllers.Application.LoginData
import utils.SignupError.SignupError
import utils._
import models.User
import play.api._
import play.api.mvc._
import play.api.libs.json._
import play.api.libs.functional.syntax._
import utils.{ErrorMessage, Utils}

class Application extends Controller {
  import Application._

  val accountService = AccountService()

  def index = Action {
    Ok(views.html.index())
  }

  def login = Action {
    Ok(views.html.login())
  }

  def signup = Action {
    Ok(views.html.signup())
  }

  def loginPost = Action(BodyParsers.parse.json) { request =>
    request.body.validate[Application.LoginData].fold(
      error => BadRequest(ErrorMessage.wrongFormat),
      loginData => {
        println("LoginPost. login: " + loginData.login + "; password: " + loginData.password)
        accountService.getUserBySession(Utils.getSessionID(request)) match {
          case Some(user: User) => Ok(ErrorMessage.alreadyAuthorised)
          case _ =>
            accountService.login(loginData.login, loginData.password) match {
              case Some(sessionId: String) =>
                Ok(Json.obj(
                  "login" -> loginData.login
                )).withSession(request.session + (Utils.sessionId -> sessionId))
              case _ => Ok(ErrorMessage.wrongAuth)
            }
        }
      }
    )
  }

  def signupPost = Action(BodyParsers.parse.json) { request =>
    request.body.validate[Application.SignupData].fold(
      error => BadRequest(ErrorMessage.wrongFormat),
      signupData => {
        println("SignupPost. login: " + signupData.login + "; password: " + signupData.password)
        accountService.getUserBySession(Utils.getSessionID(request)) match {
          case Some(user: User) => Ok(ErrorMessage.alreadyAuthorised)
          case _ =>
            accountService.signup(signupData.login, signupData.password) match {
              case Some(sessionId: String) =>
                Ok(
                  Json.obj("login" -> signupData.login)
                ).withSession(request.session + (Utils.sessionId -> sessionId))
              case Some(signupError: SignupError) =>
                Ok(
                  signupError match {
                    case SignupError.loginIsNotAvailable => ErrorMessage.suchUserExist
                    case SignupError.loginTooShort => ErrorMessage.wrongFormat
                    case SignupError.passwordTooShort => ErrorMessage.wrongFormat
                  }
                )
              case _ => Ok(ErrorMessage.errorWhileHandlingRequest)
            }
        }
      }
    )
  }

  def getUser = Action { request =>
    println("GetUser")
    accountService.getUserBySession(Utils.getSessionID(request)) match {
      case Some(user: User) =>
        println("User: " + user.toString)
        Ok(Json.toJson(user))
      case _ =>
        println("Not authorized")
        Ok(ErrorMessage.notAuthorised)
    }
  }

  def logout = Action { request =>
    println("Logout")
    Ok(
      Utils.getSessionID(request) match {
        case Some(sessionId: String) =>
          accountService.removeSession(sessionId)
          println("SessionId: " + sessionId)
          Json.obj("result" -> "success")
        case _ => ErrorMessage.notAuthorised
      }
    ).withNewSession
  }
}

object Application {
  case class LoginData(login: String, password: String)
  case class SignupData(login: String, password: String)

  implicit val loginDataReads: Reads[LoginData] = (
    (JsPath \ "login") .read[String] and
      (JsPath \ "password").read[String]
    )(LoginData.apply _)

  implicit val signupDataReads: Reads[SignupData] = (
    (JsPath \ "login") .read[String] and
      (JsPath \ "password").read[String]
    )(SignupData.apply _)

  implicit val userWrites: Writes[User] = (
    (__ \ "login").write[String] and
      (__ \ "password").write[String]
    )(unlift((user: User) => Some(user.login, user.password)))
}