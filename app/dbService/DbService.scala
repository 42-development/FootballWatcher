package dbService

import models.User

/**
  * Created by neikila.
  */
trait DbService {
  def getUser(login: String): Option[User]

  def saveUser(user: User): Boolean
}
