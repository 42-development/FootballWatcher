package dbService

import models.User

import scala.collection.immutable.HashMap

// Заглушка
class DbServiceStubImpl extends DbService {
  var mapLoginToUser = new HashMap[String, User]

  override def getUser(login: String): Option[User] = mapLoginToUser.get(login)

  override def saveUser(user: User): Boolean = {
    val haveAlreadyExisted = mapLoginToUser.contains(user.login)
    if (!haveAlreadyExisted)
      mapLoginToUser += (user.login -> user)
    !haveAlreadyExisted
  }
}
