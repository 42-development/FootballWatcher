package accountService

import dbService.DbServiceStubImpl
import models.User
import utils.SignupError

import scala.collection.immutable.HashMap

class AccountService {
  val dbService = new DbServiceStubImpl

  var mapSessionToLogin = new HashMap[String, String]
  var mapLoginToSession = new HashMap[String, String]

  // Результат: Option[сессионный ключ]
  def login(login: String, password: String): Option[String] = {
    dbService.getUser(login) match {
      case Some(user: User) =>
        if (user.password == password) {
          val sessionId = createSessionId(user)
          mapSessionToLogin += (sessionId -> user.login)
          mapLoginToSession += (user.login -> sessionId)
          Some(sessionId)
        } else {
          None
        }
      case _ => None
    }
  }

  def signup(login: String, password: String): Option[Any] = {
    dbService.saveUser(new User(login, password)) match {
      case true => this.login(login, password)
      case _ => Some(SignupError.loginIsNotAvailable)
    }
  }

  def getUserBySession(sessionIdOption: Option[String]): Option[User] = {
    sessionIdOption match {
      case Some(sessionId: String) =>
        if (mapSessionToLogin.contains(sessionId)) dbService.getUser(mapSessionToLogin(sessionId)) else None
      case _ => None
    }
  }

  def createSessionId(user: User): String = {
    user.login
  }

  def removeSession(sessionId: String) = {
    if (mapSessionToLogin.contains(sessionId)) {
      val login = mapSessionToLogin(sessionId)
      mapSessionToLogin -= sessionId
      mapLoginToSession -= login
    }
  }

  def developmentFilling() = {
    dbService.saveUser(new User("test", "test"))
  }
}

object AccountService {
  val accountService = { val temp = new AccountService; temp.developmentFilling(); temp }

  def apply(): AccountService = accountService
}
