package utils

object SignupError extends Enumeration {
  type SignupError = Value
  val loginTooShort, passwordTooShort, loginIsNotAvailable = Value
}
