package utils

import play.api.libs.json.Json

object ErrorMessage {
  private val notAuthorisedCode = 1
  private val alreadyAuthorisedCode = 2
  private val errorWhileHandlingRequestCode = 3
  private val wrongFormatCode = 4
  private val wrongAuthCode = 5
  private val suchUserExistCode = 6
  private val passwordsDifferCode = 7

  val notAuthorised = {
    Json.obj(
      "error" -> "Not authorised",
      "code" -> notAuthorisedCode
    )
  }

  val alreadyAuthorised = {
    Json.obj(
      "error" -> "Already authorised",
      "code" -> alreadyAuthorisedCode
    )
  }

  val errorWhileHandlingRequest = {
    Json.obj(
      "error" -> "Server error",
      "code" -> errorWhileHandlingRequestCode
    )
  }

  val wrongFormat = {
    Json.obj(
      "error" -> "Wrong request format",
      "code" -> wrongFormatCode
    )
  }

  def wrongFormat(extraMessage: String) = {
    Json.obj(
      "error" -> "Wrong request format",
      "extraMessage" -> extraMessage,
      "code" -> wrongFormatCode
    )
  }

  val wrongAuth = {
    Json.obj(
      "error" -> "Wrong pass or login",
      "code" -> wrongAuthCode
    )
  }

  val suchUserExist = {
    Json.obj(
      "error" -> "This login isn't available",
      "code" -> suchUserExistCode
    )
  }

  val passwordsDiffer = {
    Json.obj(
      "error" -> "Passwords differ",
      "code" -> passwordsDifferCode
    )
  }
}
