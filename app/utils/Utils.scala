package utils

import play.api.libs.json.JsValue
import play.api.mvc.{AnyContent, Request}

object Utils {
  val sessionId = "sessionId"

  def getSessionID(request: Request[Any]): Option[String] = {
    request.session.get(sessionId)
  }
}
